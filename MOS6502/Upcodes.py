class MOS6502_UPCODES:
    def __init__(self, M):
        self.MOS6502_UPCODES = [
            M.BRK, M.ORA, None    , None , None , M.ORA, M.ASL, None,  # 00
            M.PHP, M.ORA, M.ASL_A , None , None , M.ORA, M.ASL, None,  # 08
            M.BPL, M.ORA, None    , None , None , M.ORA, M.ASL, None,  # 10
            M.CLC, M.ORA, None    , None , None , M.ORA, M.ASL, None,  # 18
            M.JSR, M.AND, None    , None , M.BIT, M.AND, M.ROL, None,  # 20
            M.PLP, M.AND, M.ROL_A , None , None , M.AND, M.ROL, None,  # 28
            M.BMI, M.AND, None    , None , None , M.AND, M.ROL, None,  # 30
            M.SEC, M.AND, None    , None , None , M.AND, M.ROL, None,  # 38
            M.RTI, M.EOR, None    , None , None , M.EOR, M.LSR, None,  # 40
            M.PHA, M.EOR, M.LSR_A , None , M.JMP, M.EOR, M.LSR, None,  # 48
            M.BVC, M.EOR, None    , None , None , M.EOR, M.LSR, None,  # 50
            M.CLI, M.EOR, None    , None , None , M.EOR, M.LSR, None,  # 58
            M.RTS, M.ADC, None    , None , None , M.ADC, M.ROR, None,  # 60
            M.PLA, M.ADC, M.ROR_A , None , M.JMP, M.ADC, M.ROR, None,  # 68
            M.BVS, M.ADC, None    , None , None , M.ADC, M.ROR, None,  # 70
            M.SEI, M.ADC, None    , None , None , M.ADC, M.ROR, None,  # 78
            None , M.STA, None    , None , M.STY, M.STA, M.STX, None,  # 80
            M.DEY, None , M.TXA   , None , M.STY, M.STA, M.STX, None,  # 88
            M.BCC, M.STA, None    , None , M.STY, M.STA, M.STX, None,  # 90
            M.TYA, M.STA, M.TXS   , None , None , M.STA, None , None,  # 98
            M.LDY, M.LDA, M.LDX   , None , M.LDY, M.LDA, M.LDX, None,  # a0
            M.TAY, M.LDA, M.TAX   , None , None , M.LDA, M.LDX, None,  # a8
            M.BCS, M.LDA, None    , None , M.LDY, M.LDA, M.LDX, None,  # b0
            M.CLV, M.LDA, M.TSX   , None , M.LDY, M.LDA, M.LDX, None,  # b8
            M.CPY, M.CMP, None    , None , M.CPY, M.CMP, M.DEC, None,  # c0
            M.INY, M.CMP, M.DEX   , None , M.CPY, M.CMP, M.DEC, None,  # c8
            M.BNE, M.CMP, None    , None , None , M.CMP, M.DEC, None,  # d0
            M.CLD, M.CMP, None    , None , None , M.CMP, M.DEC, None,  # d8
            M.CPX, M.SEC, None    , None , M.CPX, M.SBC, M.INC, None,  # e0
            M.INX, M.SEC, M.NOP   , None , M.CPX, M.SBC, M.INC, None,  # e8
            M.BEW, M.SEC, None    , None , None , M.SBC, M.INC, None,  # f0
            M.SED, M.SEC, None    , None , None , M.SBC, M.INC, None   # f8
        ]

    def getUpcodes(self):
        return self.MOS6502_UPCODES

    def __repr__(self):
        upcodes = ""
        for i in range(255):
            upcodes = upcodes + self.getUpcodes()[i]
            if(not (i % 16)):
                upcodes = upcodes + "\n"


