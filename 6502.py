from MOS6502.Upcodes import MOS6502_UPCODES
from MOS6502.Statuses import MOS6502_STATUSES


class MOS6502Chip:
    class UPCODE_ADDR(object):
        def __init__(self, f):
            self.f = f

        def __call__(self, *args, **kwargs):
            MOS = args[0]
            x = MOS.x
            y = MOS.y
            if self.check_abs(args[1]):  # _ABS functies
                addr = MOS.getAddress(self=MOS)
            elif self.check_abs_x(args[1]):  # _ABS_X functies
                addr = MOS.getAddress(self=MOS) + x
            elif self.check_abs_y(args[1]):  # _ABS_Y functies
                addr = MOS.getAddress(self=MOS) + y
            elif self.check_zpg(args[1]):  # _ZPG functies
                addr = MOS.readByte(self=MOS)
            elif self.check_zpg_x(args[1]):  # _ZPG_X functies
                addr = MOS.readByte(self=MOS) + x
            elif self.check_zpg_y(args[1]):  # _ZPG_Y functies
                addr = MOS.readByte(self=MOS) + y
            elif self.check_ind_x(args[1]):  # _ZPG functies
                addr = MOS.readByte(self=MOS)
            elif self.check_ind_y(args[1]):  # _ZPG_X functies
                addr = MOS.readByte(self=MOS) + x
            else:
                addr = -0x01
            self.f(self=MOS, addr=addr)

        @staticmethod
        def check_abs(byte):
            return (byte % 0x20) in [0x0C, 0x0D, 0x0E] or byte is 0x20 and (byte not in [0x0C, 0x6C])

        @staticmethod
        def check_abs_x(byte):
            return (byte % 0x20) in [0x1D, 0x1E] or byte is 0xBC and byte not in [0x9E, 0xBE]

        @staticmethod
        def check_abs_y(byte):
            return (byte % 0x20) in [0x19, 0x1E] or byte is 0xBE

        @staticmethod
        def check_zpg(byte):
            return (byte % 0x20) in [0x05, 0x06, 0x07] and (byte not in [0x04, 0x44, 0x64])

        @staticmethod
        def check_zpg_x(byte):
            return (byte % 0x20) in [0x15, 0x16] or byte in [0x94, 0xB4] and byte not in [0x96, 0xB6]

        @staticmethod
        def check_zpg_y(byte):
            return byte in [0x96, 0xB6]

        @staticmethod
        def check_ind_x(byte):
            return (byte % 0x20) in [0x01]

        @staticmethod
        def check_ind_y(byte):
            return (byte % 0x20) in [0x11]

    def __init__(self):
        self.accumulator = 0x00
        self.x = 0x00  # -- X Register
        self.y = 0x00  # -- Y Register
        self.sp = 0x01ff  # -- Stackpointer
        self.pc = 0x0600  # -- Programcounter
        self.sr = 0x20  # -- Status register
        self.memory = [0x00] * 0x10000
        self.upcodes = MOS6502_UPCODES(self).getUpcodes()

    def writeIntoMemory(self, start, data):
        for i in range(len(data)):
            self.memory[start + i] = data[i]

    def memdump(self, start=0x00, stop=0x10000):
        memdata = "     "
        for j in range(0, 32):
            memdata += hex(j)[2:].zfill(2) + " "
        memdata += "\n"
        for i in range(0, 2048):
            memdata += hex(i * 32)[2:].zfill(4) + " "
            for j in range(0, 32):
                memdata += hex(self.memory[i * 32 + j])[2:].zfill(2) + " "
            memdata += "\n"
        return memdata

    def regdump(self):
        registers = ""
        registers += "AC: " + bin(self.accumulator)[2:].zfill(8)
        registers += "\nX : " + bin(self.x)[2:].zfill(8)
        registers += "\nY : " + bin(self.y)[2:].zfill(8)
        registers += "\nSP: " + bin(self.sp)[2:].zfill(16)
        registers += "\nPC: " + bin(self.pc)[2:].zfill(16)
        registers += "\nSR: " + bin(self.sr)[2:].zfill(8)
        return registers

    def run(self):
        while not self.getStatusFlag(MOS6502_STATUSES.BREAK):
            byte = self.readByte()
            self.upcodes[byte](self=self, addr=byte)

    def readByte(self):
        byte = self.memory[self.pc]
        self.pc += 0x01
        return byte

    def getAddress(self):
        return self.readByte() + (self.readByte() * 0x0100)

    def setStatusFlag(self, flag):
        self.sr = self.sr | flag

    def getStatusFlag(self, flag):
        return self.sr & flag

    def resetStatusFlag(self, flag):
        self.sr = self.sr & (~flag)

    def increment(self, addr):
        self.RAM_write(addr, self.RAM_read(addr) + 1)
        if self.RAM_read(addr) > 0xff:
            self.RAM_write(addr, self.RAM_read(addr) & 0xff)
            self.setStatusFlag(MOS6502_STATUSES.OVERFLOW)

    def checkASL(self, newValue, oldVal):
        if not newValue: self.setStatusFlag(MOS6502_STATUSES.ZERO)
        if oldVal & 0x80: self.setStatusFlag(MOS6502_STATUSES.CARRY)
        if newValue & 0x80: self.setStatusFlag(MOS6502_STATUSES.NEGATIVE)

    def checkLSR(self, newValue, oldVal):
        if not newValue: self.setStatusFlag(MOS6502_STATUSES.ZERO)
        if oldVal & 0x01: self.setStatusFlag(MOS6502_STATUSES.CARRY)
        if newValue & 0x80: self.setStatusFlag(MOS6502_STATUSES.NEGATIVE)

    def RAM_read(self, addr):
        return self.memory[addr]

    def RAM_write(self, addr, data):
        self.memory[addr] = data

    def SP_push(self, data):
        self.RAM_write(self.sp, data)
        self.sp -= 1

    def SP_pop(self):
        self.sp += 1
        return self.RAM_read(self.sp)

    def branch_on(self, bool):
        BB = self.readByte()
        if bool:
            self.pc += BB

    @UPCODE_ADDR
    def ORA(self, addr):
        if addr is not -0x01:
            self.accumulator |= self.RAM_read(addr)
        else:
            self.accumulator |= addr

    @UPCODE_ADDR
    def ASL(self, addr):
        oldVal = self.RAM_read(addr)
        self.RAM_write(addr, oldVal << 1)
        self.checkASL(self.RAM_read(addr), oldVal)

    @UPCODE_ADDR
    def JSR(self, addr):
        self.SP_push(addr >> 8 & 0xff)
        self.SP_push(addr & 0xff)
        self.pc = addr

    @UPCODE_ADDR
    def AND(self, addr):
        if addr is not -0x01:
            self.accumulator &= self.RAM_read(addr)
        else:
            self.accumulator &= addr

    @UPCODE_ADDR
    def BIT(self, addr):
        if not self.RAM_read(addr):
            self.setStatusFlag(MOS6502_STATUSES.ZERO)
        if self.RAM_read(addr) & 0x80:
            self.setStatusFlag(MOS6502_STATUSES.NEGATIVE)
        if self.RAM_read(addr) & 0x40:
            self.setStatusFlag(MOS6502_STATUSES.OVERFLOW)

    @UPCODE_ADDR
    def ROL(self, addr):
        oldVal = self.RAM_read(addr)
        self.RAM_write(addr, oldVal << 1 + self.getStatusFlag(MOS6502_STATUSES.CARRY))
        self.CLC()
        self.checkASL(self.RAM_read(addr), oldVal)

    @UPCODE_ADDR
    def ROR(self, addr):
        oldVal = self.RAM_read(addr)
        self.RAM_write(addr, oldVal >> 1 + (self.getStatusFlag(MOS6502_STATUSES.CARRY * 0x80)))
        self.CLC()
        self.checkLSR(self.RAM_read(addr), oldVal)

    @UPCODE_ADDR
    def EOR(self, addr):
        if addr is not -0x01:
            self.accumulator ^= self.RAM_read(addr)
        else:
            self.accumulator ^= addr

    @UPCODE_ADDR
    def LSR(self, addr):
        oldVal = self.RAM_read(addr)
        self.RAM_write(addr, oldVal >> 1)
        self.checkLSR(self.RAM_read(addr), oldVal)

    @UPCODE_ADDR
    def ADC(self, addr):
        oldVal = self.accumulator
        if addr is not -0x01:
            self.accumulator += self.RAM_read(addr) + self.getStatusFlag(MOS6502_STATUSES.CARRY)
        else:
            self.accumulator += addr + self.getStatusFlag(MOS6502_STATUSES.CARRY)
        self.CLC()
        if oldVal > self.accumulator: self.setStatusFlag(MOS6502_STATUSES.CARRY)
        if not self.accumulator: self.setStatusFlag(MOS6502_STATUSES.ZERO)
        if self.accumulator & 0x80: self.setStatusFlag(MOS6502_STATUSES.NEGATIVE)

    @UPCODE_ADDR
    def JMP(self, addr):
        self.pc = addr

    def ASL_A(self):
        oldVal = self.accumulator
        self.accumulator <<= 1
        self.checkASL(self.accumulator, oldVal)

    def ROL_A(self):
        oldVal = self.accumulator
        self.accumulator <<= 1 + self.getStatusFlag(MOS6502_STATUSES.CARRY)
        self.CLC()
        self.checkASL(self.accumulator, oldVal)

    def ROR_A(self, addr):
        oldVal = self.RAM_read(addr)
        self.accumulator = (self.accumulator >> 1) + (self.getStatusFlag(MOS6502_STATUSES.CARRY) * 0x80)
        self.CLC()
        self.checkLSR(self.RAM_read(addr), oldVal)

    def LSR_A(self):
        oldVal = self.accumulator
        self.accumulator >>= 1
        self.checkLSR(self.accumulator, oldVal)

    def RTS(self):
        LO = self.SP_pop()
        HI = self.SP_pop()
        self.pc = (HI << 8 | LO)

    def RTI(self):
        pass

    def BVC(self):
        self.branch_on(not self.getStatusFlag(MOS6502_STATUSES.OVERFLOW))

    def BVS(self):
        self.branch_on(self.getStatusFlag(MOS6502_STATUSES.OVERFLOW))

    def BNE(self):
        self.branch_on(not self.getStatusFlag(MOS6502_STATUSES.ZERO))

    def BEQ(self):
        self.branch_on(self.getStatusFlag(MOS6502_STATUSES.ZERO))

    def BMI(self):
        self.branch_on(self.getStatusFlag(MOS6502_STATUSES.NEGATIVE))

    def BPL(self):
        self.branch_on(not self.getStatusFlag(MOS6502_STATUSES.NEGATIVE))

    def BRK(self):
        self.setStatusFlag(MOS6502_STATUSES.BREAK)

    def PHP(self):
        self.memory[self.sp] = self.sr
        self.sp -= 1

    def PLP(self):
        self.sp += 1
        self.sr = self.memory[self.sp]

    def PHA(self):
        self.memory[self.sp] = self.accumulator
        self.sp -= 1

    def PLA(self):
        self.sp += 1
        self.accumulator = self.memory[self.sp]

    def SEC(self):
        self.setStatusFlag(MOS6502_STATUSES.CARRY)

    def SED(self):
        self.setStatusFlag(MOS6502_STATUSES.DECIMAL)

    def SEI(self):
        self.setStatusFlag(MOS6502_STATUSES.INTERRUPT)

    def CLC(self):
        self.resetStatusFlag(MOS6502_STATUSES.CARRY)

    def CLI(self):
        self.resetStatusFlag(MOS6502_STATUSES.INTERRUPT)

    def DEY(self):
        self.y -= 1
        if self.y < 0:
            self.setStatusFlag(MOS6502_STATUSES.NEGATIVE)

    def TYA(self):
        self.y = self.accumulator

    def TAY(self):
        self.accumulator = self.y


def main():
    mos = MOS6502Chip()
    mos.writeIntoMemory(0x0600, [0x08])
    mos.run()
    print(mos.memdump())
    print(mos.regdump())


if __name__ == "__main__":
    main()
